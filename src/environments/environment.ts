// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAvvvu0S0_5EmFBdP2a-cVgjP9J6lYRqik",
    authDomain: "fitness-tracker-d40ec.firebaseapp.com",
    databaseURL: "https://fitness-tracker-d40ec.firebaseio.com",
    projectId: "fitness-tracker-d40ec",
    storageBucket: "fitness-tracker-d40ec.appspot.com",
    messagingSenderId: "1080372068750",
    appId: "1:1080372068750:web:a2308be5817519f767a4df"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
