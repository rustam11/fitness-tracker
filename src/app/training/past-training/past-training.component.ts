import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from "@angular/material";

import {Store} from '@ngrx/store';

import * as fromTraining from '../training.reducer';
import {Exercise} from "../exercise.model";
import {TrainingService} from "../training.service";
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-past-training',
  templateUrl: './past-training.component.html',
  styleUrls: ['./past-training.component.scss']
})

export class PastTrainingComponent implements OnInit, AfterViewInit {
  displayedColumns = ['date', 'name', 'duration', 'calories', 'state'];
  dataSource = new MatTableDataSource<Exercise>();
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(
    private trainingsService: TrainingService,
    private store: Store<fromTraining.State>
  ) {
  }

  ngOnInit() {
    this.store.select(fromTraining.getFinishedExercises)
      .pipe( map(resData => {
          return resData.map((exercise: any) => {
            return {
              id: exercise.id,
              name: exercise.name,
              duration: exercise.duration,
              calories: exercise.calories,
              date: new Date(exercise.date.seconds * 1000),
              state: exercise.state
            }
          })
        })
      )
      .subscribe(finishedEx => {
        this.dataSource.data = finishedEx
      });
    this.trainingsService.fetchCompletedOrCanceledExercises();
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  doFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }
}
