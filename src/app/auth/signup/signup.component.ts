import {Component, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";

import {Store} from '@ngrx/store';

import * as frommRoot from '../../app.reducer'
import {AuthService} from "../auth.service";
import {Observable} from "rxjs";
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  maxDate;
  isLoading$: Observable<boolean>;

  constructor(private authService: AuthService, private store: Store<frommRoot.State>) {
  }

  ngOnInit() {
    this.isLoading$ = this.store.select(frommRoot.getIsLoading);
    this.maxDate = new Date();
    this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);

  }

  onSubmit(form: NgForm) {
    this.authService.registerUser({
      email: form.value.email,
      password: form.value.password
    });
  }
}
