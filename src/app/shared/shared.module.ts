import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MaterialModule} from "../material.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  imports:
    [
      CommonModule,
      MaterialModule,
      FlexLayoutModule,
      ReactiveFormsModule,
      FormsModule
    ],
  exports:
    [
      CommonModule,
      MaterialModule,
      FlexLayoutModule,
      ReactiveFormsModule,
      FormsModule
    ]
})

export class SharedModule {


}
